# Email obfuscator

Obfuscate email addresses by shuffling characters and rearrange by using flexbox order.

## Installation

```bash
composer require balping/email-obfuscator
```

## Example

```php 
echo obfuscateEmail("info@example.com");
```

This outputs characters in random order. However, CSS can rearrange them in original order using [order](https://developer.mozilla.org/en-US/docs/Web/CSS/order):

```html
<span style="display: inline-flex; user-select: none;">
    <span style="order:14;">o</span>
    <span style="order:15;">m</span>
    <span style="order:13;">c</span>
    <span style="order: 5;">e</span>
    <span style="order:10;">l</span>
    <span style="order: 7;">a</span>
    <span style="order: 8;">m</span>
    <span style="order: 6;">x</span>
    <span style="order:12;">.</span>
    <span style="order: 0;">i</span>
    <span style="order: 9;">p</span>
    <span style="order: 4;">@</span>
    <span style="order: 1;">n</span>
    <span style="order: 2;">f</span>
    <span style="order: 3;">o</span>
    <span style="order:11;">e</span>
</span>
```

To prevent the user copy&pasting a wrong email, `user-select: none;` is applied to the displayed email address.

## License

This library is licensed under GPLv3.