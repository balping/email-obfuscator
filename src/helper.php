<?php

/*

Email obfuscator: Obfuscate email addresses by shuffling characters
Copyright (C) 2018  Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


function obfuscateEmail(string $email): string {
	retry:
	$characters = str_split($email);
	foreach($characters as $index => $c){
		$characters[$index] = [$index, $c];
	}

	shuffle($characters);

	$shuffled = '';
	$obfuscated = '<span style="display:inline-flex;align-items: baseline;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;">';
	foreach($characters as $index => $tupple){
		$obfuscated .= "<span style=\"order:${tupple[0]};\">${tupple[1]}</span>";
		$shuffled .= $tupple[1];
	}
	$obfuscated .= '</span>';

	// re-shuffle if the shuffled email is the same as the original
	if(
		$shuffled == $email
		&& count(array_unique(str_split($email))) >= 2 // avoid infinite loop
	){
		goto retry;
	}

	return $obfuscated;
}

?>